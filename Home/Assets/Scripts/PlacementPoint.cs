﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementPoint : MonoBehaviour {

    public Transform placementPoint;
    public string roomID; // This is set by the room when it starts
    public styleType style; // This is set by the room when it starts
    public GameObject currentObject;
    public GameObject targetObject; 

	// Use this for initialization
	void Start () {
        currentObject = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
