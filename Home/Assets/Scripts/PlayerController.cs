﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Pawn pawn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        // If we haven't started yet, don't move
        if (!GameManager.instance.started) return;

        // Movement from input
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        pawn.Move(moveDirection);

        // NOTE:  Return to start is actually in the OverlayManager script. Sorry! 
        //        QUit is in Game Manager... lol!

        // Pickup
        if (Input.GetButtonDown("Pickup"))
        {
            pawn.Pickup();
        }

        // Toggles for 
        if (Input.GetButtonDown("UIToggle"))
        {
            GameManager.instance.ToggleTopView();
        }

	}
}
