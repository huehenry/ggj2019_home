﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineAddons : MonoBehaviour {

    public LayerMask frontYardLayer;
    public LayerMask noFrontYardLayer;

    public void ShowFrontYard (ICinemachineCamera inCam, ICinemachineCamera outCam)
    {
        Debug.Log("ShowFront");
        Camera.main.cullingMask = frontYardLayer;
    }

    public void HideFrontYard(ICinemachineCamera inCam, ICinemachineCamera outCam)
    {
        Debug.Log("HideFront");
        Camera.main.cullingMask = noFrontYardLayer;
    }

}
