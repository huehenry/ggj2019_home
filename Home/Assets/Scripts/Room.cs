﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {

    public List<PlacementPoint> placementPoints;
    public styleType style;

	// Use this for initialization
	void Start () {
        GetPlacementPoints();
        UpdatePlacementPoints();
    }

    public void GetPlacementPoints()
    {
        placementPoints = new List<PlacementPoint>(gameObject.GetComponentsInChildren<PlacementPoint>());
        // Debug.Log("Room has its placement points");
    }

    public void UpdatePlacementPoints ()
    {
        // Tell the placement points they are in this room -- so they need its name and style
        foreach (PlacementPoint pp in placementPoints)
        {
            pp.roomID = name;
            pp.style = style;
            //Debug.Log("Updating " + pp.name + " to " + pp.style);
        }
    }
	
	// Update is called once per frame
	void Update () {
    }
}
