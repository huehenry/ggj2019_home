﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YardTrigger : MonoBehaviour {
    public LayerMask frontYardLayer;
    public LayerMask noFrontYardLayer;

    public void ShowFrontYard()
    {
        //Debug.Log("ShowFront");
        Camera.main.cullingMask = frontYardLayer;
    }

    public void HideFrontYard()
    {
        //Debug.Log("HideFront");
        Camera.main.cullingMask = noFrontYardLayer;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameManager.instance.player)
        {
            ShowFrontYard();
        }
    }

    private void OnTriggerExit(Collider other)       
    {
        //Debug.Log(other.name);
        if (other.gameObject == GameManager.instance.player)
        {
            HideFrontYard();
        }
    }

}
