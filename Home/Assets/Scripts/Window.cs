﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : PlacementPoint
{

    public PlacementPoint[] placementPoints;    //Possible Placements for Object (lawn)

    // Use this for initialization
    void Start()
    {
        currentObject = null;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public PlacementPoint CheckForPlacement()   //Checks to see if there's space to throw out window
    {
        foreach (PlacementPoint p in placementPoints)   //Search all Placement points on lawn
        {
            if (p.currentObject == null)    //If there's an opening
            {

                return p; //Place there
            }
        }
        return null;    //If there's no space, you can't place
    }
}
