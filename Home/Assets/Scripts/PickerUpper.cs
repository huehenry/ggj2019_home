﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Any object with this component (requires a collider) will:
//     Track any object that enters its collider
//     Track the object the player is carrying
//     Move "object in hand" (if not null) to carry point.
//
//
//     When it is told (by the controller) to do work it will do:
//          (Glow) last object in the collider (if possible)
//          Pickup the object (put in hand)

public class PickerUpper : MonoBehaviour {

    public List<GameObject> objectsInCollider;
    public GameObject objectInHand;
    public Transform carryPoint;
    public Collider pickupCollider;

	private GameObject lastObjectInHand;

    // Use this for initialization
    void Start () {
        SFXManager._sfx = FindObjectOfType<SFXManager>();

		if (pickupCollider == null)
        {
            GetComponent<Collider>();
            if (pickupCollider == null)
            {
                Debug.LogError("ERROR: NO COLLIDER FOR PICKERUPPER");
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		// TEST: 
        AllObjectsOff();
        ShowClosestObject();

        // If we have an object, keep it in our mouth
        if (objectInHand)
        {
            objectInHand.transform.position = carryPoint.position;
			objectInHand.transform.localRotation = carryPoint.transform.localRotation;
			GameManager.instance.animator.mouthOpen = true;
			if (lastObjectInHand != objectInHand)
			{
				lastObjectInHand = objectInHand;
				OverlayManager._UI.itemName.text = string.Format("{0}", lastObjectInHand.name);
			}
		}
		else
		{
			GameManager.instance.animator.mouthOpen = false;
			OverlayManager._UI.itemName.text = "";
			lastObjectInHand = null;
		}
	}

    public void TogglePickup ()
    {
        bool isWindow = false;
        GameObject temp = null;

        // If there is an object in hand, check for placement point
        if (objectInHand != null)
        {
            GameObject closestObject = ClosestObject();
            if (closestObject != null)
            {
                PlacementPoint target = ClosestObject().GetComponent<PlacementPoint>();

                if (target.gameObject.GetComponent<Window>() != null)
                {
                    isWindow = true;
                    target = target.gameObject.GetComponent<Window>().CheckForPlacement();
                }

                if (target != null)
                {
                    // Move object to 
                    objectInHand.transform.position = target.placementPoint.position;
                    objectInHand.transform.localRotation = target.placementPoint.localRotation;

                    // Swap objects
                    if (target.currentObject != null) {
                        // Put target object into temp, so it can be moved later
                        temp = target.currentObject;
                    }


                    //Play Sound
                    if (isWindow)
                    {
                        SFXManager._sfx.PlayEffect("Window");
                    }
                    else if (target.roomID == "Box")
                    {
                        SFXManager._sfx.PlayEffect("Box");
                    }
                    else if (target.roomID == "Lawn")
                    {
                        SFXManager._sfx.PlayEffect("Lawn");
                    }
                    else
                    {
                        SFXManager._sfx.PlayEffect("PutDown");
                    }

                    // Place knows 
                    target.currentObject = objectInHand;

                    // If we have a temp, that means we were swapping
                    if (temp != null) {
                        objectInHand = temp;
                    }
                    else {
                        // Otherwise, just Drop object
                        objectInHand = null;
                    }   
                }
                else
                {
                    SFXManager._sfx.PlayEffect("Speak");
                }
            }
            else
            {
                SFXManager._sfx.PlayEffect("Speak");
            }
        }
        else {
            // If not, check for object
            GameObject closestObject = ClosestObject();
            if (closestObject != null)
            {
                PickupObject target = ClosestObject().GetComponent<PickupObject>();
                if (target != null)
                {
                    // Move object to mouth point
                    target.transform.position = carryPoint.position;
                    SFXManager._sfx.PlayEffect("PickUp");

                    //Pickup object
                    objectInHand = target.gameObject;

                    // WORST CODE EVER -- NEVER DO THIS
                    // Iterate through all placement points, remove this object from them -- If we are carrying it, its not in any placement point.
                    foreach (PlacementPoint pp in GameManager.instance.placementPoints) {
                        if (pp.currentObject == objectInHand) {
                            pp.currentObject = null;
                        }
                    }

                    // Unglow it
                    Glower glower = objectInHand.GetComponent<Glower>();
                    if (glower != null)
                    {
                        glower.Unglow();
                    }
                }
            }
            else
            {
                SFXManager._sfx.PlayEffect("Speak");
            }
        }
    }

    

    void OnTriggerEnter(Collider other)
    {
        // Note: Only add objects that are pickups or placepoints
        PickupObject po = other.GetComponent<PickupObject>();
        PlacementPoint pp = other.GetComponent<PlacementPoint>();
        if (po != null || pp != null)
        {
            objectsInCollider.Add(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        objectsInCollider.Remove(other.gameObject);

        // Turn off
        Glower glower = other.GetComponent<Glower>();
        if (glower != null)
        {
            glower.Unglow();
        }

    }

    public void ShowLastObject()
    {
        // If no objects, quit;
        if (objectsInCollider == null || objectsInCollider.Count == 0) return;

        // All objects off
        AllObjectsOff();

        // Turn on last object in list
        GameObject target = objectsInCollider[objectsInCollider.Count - 1];
        Glower glower = target.GetComponent<Glower>();
        if (glower != null)
        {
            glower.Glow();
        }        
    }


    public void ShowClosestObject()
    {
        // If no objects, quit;
        if (objectsInCollider == null || objectsInCollider.Count == 0) return;

        // All objects off.
        AllObjectsOff();

        // If closest can glow, make it glow
        GameObject closestObject = ClosestObject();

        if (closestObject != null)
        {
            Glower glower = closestObject.GetComponent<Glower>();
            if (glower != null)
            {
                PickupObject po = glower.GetComponent<PickupObject>();
                PlacementPoint pp = glower.GetComponent<PlacementPoint>();
                if ((po != null && objectInHand == null) || (pp != null && objectInHand != null))
                {
                    // Turn it on
                    glower.Glow();
                }
            }
        }
    }


    public PickupObject ClosestPickupObject()
    {
        // Assume first object is closest
        GameObject closestObject = null;
        float closestDistance = Mathf.Infinity;

        // Iterate through all 
        for (int i = 0; i < objectsInCollider.Count; i++)
        {
            // Only works with pickupObjects
            if (objectsInCollider[i].GetComponent<PickupObject>() == null)
            { continue; }

            // If this one is closer than closest
            float testDistance = Vector3.Distance(transform.position, objectsInCollider[i].transform.position);
            if (testDistance <= closestDistance)
            {
                // Then it is closest
                closestObject = objectsInCollider[i];
                closestDistance = testDistance;
            }
        }
        if (closestObject != null)
        {
            return closestObject.GetComponent<PickupObject>();
        } else
        {
            return null;
        }
    }

    public PlacementPoint ClosestPlacementPoint()
    {
        // Assume first object is closest
        GameObject closestObject = null;
        float closestDistance = Mathf.Infinity;

        // Iterate through all 
        for (int i = 0; i < objectsInCollider.Count; i++)
        {
            // Only works with pickupObjects
            if (objectsInCollider[i].GetComponent<PlacementPoint>() == null)
            {
                continue;
            }

            // Skip the ones that have objects
            //if (objectsInCollider[i].GetComponent<PlacementPoint>().currentObject != null)
            //{
            //    continue;
            //}

            // If this one is closer than closest
            float testDistance = Vector3.Distance(transform.position, objectsInCollider[i].transform.position);
            if (testDistance <= closestDistance)
            {
                // Then it is closest
                closestObject = objectsInCollider[i];
                closestDistance = testDistance;
            }
        }

        if (closestObject != null)
        {
            return closestObject.GetComponent<PlacementPoint>(); 
        }
        else
        {
            return null;
        }
    }

    public GameObject ClosestObject ()
    {
        if (objectInHand )
        {
            PlacementPoint pp = ClosestPlacementPoint();
            if (pp != null)
            {
                return pp.gameObject;
            } else
            {
                return null;
            }
        }
        else
        {
            PickupObject po = ClosestPickupObject();
            if (po != null)
            {
                return po.gameObject;
            }
            else
            {
                return null;
            }
        }

    }

    public void ShowAllObjects()
    {
        // If no objects, quit;
        if (objectsInCollider == null || objectsInCollider.Count == 0) return;

        // Iterate through all 
        for (int i = 0; i < objectsInCollider.Count; i++)
        {
            GameObject target = objectsInCollider[i];
            // If they have a glower
            Glower glower = target.GetComponent<Glower>();
            if (glower != null)
            {
                // Turn it on
                glower.Glow();
            }
        }
    }

    public void AllObjectsOff()
    {        
        // If no objects, quit;
        if (objectsInCollider == null || objectsInCollider.Count == 0) return;

        // Iterate through all 
        for (int i = 0; i < objectsInCollider.Count; i++)
        {
            GameObject target = objectsInCollider[i];
            // If they have a glower
            Glower glower = target.GetComponent<Glower>();
            if (glower != null)
            {
                // Turn it off
                glower.Unglow();
            }
        }
    }

}
