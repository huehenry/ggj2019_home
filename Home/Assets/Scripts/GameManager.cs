﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum styleType { SciFi, Modern, ArtDeco, Antique };

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
	public CharacterAnimation animator;
	public bool started;

    [HideInInspector] public List<PickupObject> pickupObjects;
    [HideInInspector] public List<PlacementPoint> placementPoints;
    public List<Room> rooms;

    public Camera topCamera;
    public Camera smartCamera;

    public GameObject player;
    public bool isTopView = false;

    // HORRIBLE CODE - YUCK --  1 Hour till jam ends.
    public int randomSpots = 16;



    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        BuildGame();
    }

    void Start()
    {
        LoadRandomPlacementPoints();
    }


    void BuildGame ()
    {
        pickupObjects = new List<PickupObject>(FindObjectsOfType<PickupObject>());
        placementPoints = new List<PlacementPoint>(FindObjectsOfType<PlacementPoint>());
        ResetRooms();
        AssignStylesToRooms();
        AssignObjectsToPlacementPoints();      
    }

    void LoadRandomPlacementPoints()
    {
        List<PickupObject> deckPO = new List<PickupObject>(pickupObjects);
        List<PlacementPoint> deckPP = new List<PlacementPoint>(placementPoints);

        for (int i=0; i<randomSpots; i++) {

            // Get random numbers
            int randPO = Random.Range(0, deckPO.Count);
            int randPP = Random.Range(0, deckPP.Count);

            // Place in that point

            if (deckPP[randPP].roomID != "Window" && deckPP[randPP].roomID != "Lawn" && deckPP[randPP].roomID != "Box") {
                placementPoints[placementPoints.IndexOf(deckPP[randPP])].currentObject = pickupObjects[pickupObjects.IndexOf(deckPO[randPO])].gameObject;
                deckPO[randPO].transform.position = deckPP[randPP].placementPoint.position;
				// Remove from deck
				deckPP.Remove(deckPP[randPP]);
				deckPO.Remove(deckPO[randPO]);
			}
			else
			{
				deckPP.Remove(deckPP[randPP]);
				i -= 1;
			}
            
        }




    }


    void AssignStylesToRooms()
    {
        // Make a deck of styles
        List<styleType> styleDeck = new List<styleType>();
        styleDeck.Add(styleType.Antique);
        styleDeck.Add(styleType.ArtDeco);
        styleDeck.Add(styleType.Modern);
        styleDeck.Add(styleType.SciFi);

        // For each room
        foreach (Room room in rooms)
        {
            // Get a random
            int rand = Random.Range(0, styleDeck.Count);

            // Assign a style
            room.style = styleDeck[rand];

            // Load your placement points again
            room.GetPlacementPoints();

            // Remove from deck of styles
            styleDeck.Remove(styleDeck[rand]);

            // Update the objects in the room
            room.UpdatePlacementPoints();
        }
    }

    void AssignObjectsToPlacementPoints ()
    {
        // For each room
        foreach (Room room in rooms)
        {
            // Make a deck of just the object of that style
            List<PickupObject> deck = new List<PickupObject>();

            foreach (PickupObject temp in pickupObjects)
            {
                if (temp.style == room.style)
                {
                    deck.Add(temp);
                }
            }

            foreach (PlacementPoint pu in room.placementPoints)
            {
                // Assign random object from the deck
                int rand = Random.Range(0, deck.Count);
                pu.targetObject = deck[rand].gameObject;

                // Debug.Log(pu.name + "("+pu.style+")'s target object is " + pu.targetObject.name + "("+ deck[rand].style.ToString() + ")");

                // Remove that object from the subDeck of possible objects
                deck.Remove(deck[rand]);
            }
        }




    }


    void ResetRooms()
    {
        // rooms = new List<Room>(FindObjectsOfType<Room>());
        foreach (Room room in rooms)
        {            
            for (int i = 0; i < room.placementPoints.Count; i++)
            {
                room.placementPoints[i].gameObject.SetActive(false);
            }
        }
    }


    //Toggles top view
    public void ToggleTopView()
    {
		if (started == true)
		{
			if (!isTopView)
			{
				isTopView = true;
				OverlayManager._UI.OpenOverlay();
			}
			else
			{
				isTopView = false;
				OverlayManager._UI.CloseOverlay();
			}
		}
    }

    public void HandleTopView()
    {
        if (isTopView && !topCamera.gameObject.activeInHierarchy)
        {
            topCamera.gameObject.SetActive(true);
            smartCamera.gameObject.SetActive(false);
        }
        else if (!isTopView && topCamera.gameObject.activeInHierarchy)
        {
            topCamera.gameObject.SetActive(false);
            smartCamera.gameObject.SetActive(true);
        }
    }


    void Update()
    {
        HandleTopView();

        // Check for lose!
        if (OverlayManager._UI.time <= 0) {

            // We lose So:
            // Stop playing
            started = false;
			OverlayManager._UI.endGame = true;
			// Show UI
			if (!OverlayManager._UI.open) {
                OverlayManager._UI.OpenOverlay();
            }

            //***********************************
            // TODO: Show some sort of point add on -- How did we do?  Also -- white out screen a little?
            //***********************************


            // Start button should just reload this scene to restart everything??? Do we have variables to save?
            if (Input.GetButtonDown("Start") || Input.GetKeyDown(KeyCode.Return)) {
                SceneManager.LoadScene(0);
            }

        }



        if (Input.GetButtonDown("Quit")) {
            Application.Quit();
        }
    }
}
