﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glower : MonoBehaviour
{
    public SpriteRenderer sr;
    public MeshRenderer mr;
    public Color startColor;

	// Use this for initialization
	void Start ()
	{
        if (sr == null)
        {
            sr = GetComponent<SpriteRenderer>();
        }
        if (mr == null)
        {
            mr = GetComponent<MeshRenderer>();
        }
        if (sr != null) startColor = sr.color;
        if (mr != null) startColor = mr.material.color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Glow()
    {
        // Make the object glow
        if (sr != null)
        {
            sr.color = Color.yellow;
        }
        if (mr != null)
        {
            mr.material.color = Color.yellow;
        }
    }

    public void Unglow()
    {
        // Make the object stop glowing
        if (sr != null)
        {
            sr.color = startColor;
        }
        if (mr != null)
        {
            mr.material.color = startColor;
        }
    }

    public void Antiglow()
    {
        // Make the object greyed out
        if (sr != null)
        {
            sr.color = new Color(0.1f, 0.1f, 0.1f, 0.7f);
        }
        if (mr != null)
        {
            mr.material.color = new Color(0.1f, 0.1f, 0.1f, 0.7f);
        }
    }
}
