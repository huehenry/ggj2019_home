﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterAnimation : MonoBehaviour {

	public SpriteRenderer characterFront;
	public SpriteRenderer characterBack;
	public GameObject itemattach;

	public Sprite idleMouthClosed;
	public Sprite idleFrontOpen;
	public Sprite idleBackOpen;
	public Vector3 itemAttachIdle;

	public Sprite[] spritesFrontOpen;
	public Sprite[] spritesBackOpen;
	public Sprite[] mouthClosed;
	public Vector3[] itemAttachPos;
	public float frameTime;

	public bool mouthOpen;
	private int currentFrame;
	private float timer;
	public bool animate;
	//HACK!
	public bool facingLeft;

	// Update is called once per frame
	void Update() {
		if (animate == true)
		{
			timer += Time.deltaTime;
			if (timer > frameTime)
			{
				timer = 0;
				currentFrame += 1;
				if (currentFrame == spritesFrontOpen.Length)
				{
					currentFrame = 0;
				}
			}
			if (mouthOpen == true)
			{
				characterFront.sprite = spritesFrontOpen[currentFrame];
				characterBack.sprite = spritesBackOpen[currentFrame];
			}
			else
			{
				characterFront.sprite = mouthClosed[currentFrame];
				characterBack.sprite = mouthClosed[currentFrame];
			}
			Vector3 attachPoint = itemAttachPos[currentFrame];
			if(facingLeft == true)
			{
				attachPoint.x -= .1f;
			}
			itemattach.transform.localPosition = attachPoint;
		}
		else
		{
			if (mouthOpen == true)
			{
				characterFront.sprite = idleFrontOpen;
				characterBack.sprite = idleBackOpen;
			}
			else
			{
				characterFront.sprite = idleMouthClosed;
				characterBack.sprite = idleMouthClosed;
			}
			Vector3 attachPoint = itemAttachIdle;
			if (facingLeft == true)
			{
				attachPoint.x -= .15f;
			}
			itemattach.transform.localPosition = attachPoint;
		}
	}
}
