﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {

    public PickerUpper pu;
    public Transform tf;
    public SpriteRenderer sr;
	public CharacterAnimation animator;
    public float moveSpeed = 1.0f;

    	// Use this for initialization
	void Start () {
        if (pu == null)
        {
            pu = GetComponent<PickerUpper>();
            if (pu == null)
            {
                Debug.LogWarning("WARNING: No PickerUpper on Pawn " + name);
            }
        }

        tf = GetComponent<Transform>();
        if (sr == null)
        {
            sr = GetComponent<SpriteRenderer>();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Move(Vector3 moveDirection)
    {
		if (GameManager.instance.started == true)
		{
			if (Mathf.Abs(moveDirection.x) > 0)
			{
				if (moveDirection.x > 0)
				{
					if (OverlayManager._UI.open == false)
					{
						tf.localScale = new Vector3(1.0f, 1, 1);
						animator.facingLeft = false;
					}
				}
				else
				{
					if (OverlayManager._UI.open == false)
					{
						tf.localScale = new Vector3(-1.0f, 1, 1);
						animator.facingLeft = true;
					}
				}
				if (OverlayManager._UI.open == false)
				{
					animator.animate = true;
				}
				else
				{
					animator.animate = false;
				}
			}
			else
			{
				if (Mathf.Abs(moveDirection.z) > 0 && OverlayManager._UI.open == false)
				{
					animator.animate = true;
				}
				else
				{
					animator.animate = false;
				}
				
			}
			if (OverlayManager._UI.open == false)
			{
				tf.position += moveDirection * moveSpeed * Time.deltaTime;
			}
		}
    }

    public void Pickup()
    {
        pu.TogglePickup();
    }

}
