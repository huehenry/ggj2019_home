﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour {

	[System.Serializable]
	public class SoundEffect
	{
		public string name;
		public AudioClip[] clips;
	}

	public static SFXManager _sfx;
	public SoundEffect[] soundEffects;
	private AudioSource source;

	public void Awake()
	{
		_sfx = this;
		source = this.gameObject.AddComponent<AudioSource>();
	}

	public void PlayEffect(string effectName)
	{
		foreach(SoundEffect s in soundEffects)
		{
			if(effectName == s.name)
			{
				int random = Random.Range(0, s.clips.Length);
				source.PlayOneShot(s.clips[random]);
				break;
			}
		}
	}

}
