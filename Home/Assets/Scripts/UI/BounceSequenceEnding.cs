﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceSequenceEnding : MonoBehaviour {

	public BounceInOut[] bouncers;
	float time = 0;
	public bool startSequence = false;

	private bool[] toggleOn;
	float timer;
	float ran;

	public void Awake()
	{
		toggleOn = new bool[bouncers.Length];
	}


	// Update is called once per frame
	void Update()
	{
		timer += Time.deltaTime;
		if (timer > ran)
		{
			ran = Random.Range(0.05f, 0.25f);
			timer = 0;
			int b = Random.Range(0, bouncers.Length);
			if (toggleOn[b] == true)
			{
				toggleOn[b] = false;
			}
			else
			{
				toggleOn[b] = true;
			}
		}
		if (startSequence == true)
		{
			for (int i = 0; i < bouncers.Length; i++)
			{
				if (toggleOn[i] == true)
				{
					bouncers[i].BounceIn();
				}
				else
				{
					bouncers[i].BounceOut();
				}
			}
		}
	}
}
