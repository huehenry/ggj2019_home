﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverlayManager : MonoBehaviour {

	public static OverlayManager _UI;
	public Text timer;
	public float time = 180;
	public Image fadeOut;
	public BounceSequence titleBounce;
	public BounceSequence endResultsBounce;
	public BounceSequenceEnding victoryHearts;
	public Text itemName;

	public OneRoomUI[] rooms;
	private BounceInOut[] bouncers;
	public bool open;
	private float stagger;

	public bool endGame;
	public bool victory = false;
	public Text heartCounter;

	private bool startGame;

	private void Awake()
	{
		_UI = this;
		bouncers = new BounceInOut[4];
		for(int i = 0; i<rooms.Length;i++)
		{
			bouncers[i] = rooms[i].GetComponent<BounceInOut>();
			bouncers[i].BounceOut();
			bouncers[i].lerpTimer = 1;
		}
	}

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update() {
		
		if(fadeOut.color.a <=0.6f)
		{
			titleBounce.startSequence = true;
		}
		else
		{
			fadeOut.color = Color.Lerp(fadeOut.color, new Color(1, 1, 1, 0), 3 * Time.deltaTime);
		}
		if (Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Start"))
		{
			if (startGame == false)
			{
				startGame = true;
				GameManager.instance.started = true;
			}
		}
		if (startGame)
		{
			titleBounce.goAway = true;
			fadeOut.color = Color.Lerp(fadeOut.color, new Color(1, 1, 1, 0), 3 * Time.deltaTime);

            // Subtract to keep time
            time -= Time.deltaTime;

            // Display time if started/running -- nothing if not
			int timeDisplay = (int)time;
            if (GameManager.instance.started) {
                timer.text = timeDisplay.ToString();
            } else {
                timer.text = "";
            }
		}
		if (open == true)
		{
			//Open the bouncers in a staggered fashion
			stagger += Time.deltaTime;
			bool finished = true;
			for (int i = 0; i < rooms.Length; i++)
			{
				if (stagger > 0.2f * i)
				{
					bouncers[i].BounceIn();
					if (bouncers[i].lerpTimer < 1)
					{
						finished = false;
					}
				}
			}
			if (finished == true)
			{
				//We are done opening all the UI. Now they update.
				bool allRoomsOpen = true;
				foreach (OneRoomUI r in rooms)
				{
					if (r.currentState == OneRoomUI.states.offScreen)
					{
						r.currentState = OneRoomUI.states.updateText;
					}
					if (r.currentState != OneRoomUI.states.onScreen)
					{
						allRoomsOpen = false;
					}
				}
				if (allRoomsOpen == true && endGame == true)
				{
					//LOGIC HERE DICTATES HOW MANY HEARTS THEY GOT
					endResultsBounce.startSequence = true;
					if(victory == true)
					{
						victoryHearts.startSequence = true;
					}
				}
			}
		}
		else
		{
			foreach (BounceInOut b in bouncers)
			{
				b.BounceOut();
			}
			foreach (OneRoomUI r in rooms)
			{
				if (r.currentState == OneRoomUI.states.onScreen)
				{
					r.currentState = OneRoomUI.states.offScreen;
				}
			}
		}
	}

	public void OpenOverlay()
	{
		//LOOP THROUGH ALL ITEMS HERE.
		//Create different bools and items
		for (int room = 0; room < 4; room++)
		{
			string[] names = new string[4];
			bool[] correctRooms = new bool[4];
			bool[] correctPos = new bool[4];
			bool[] correctStyle = new bool[4];
			//Cycle through the points. Get a list of the four objects that we need in order.
			//Also get a list of the four objects in the room in order
			PickupObject[] whatWeHave = new PickupObject[4];
			PickupObject[] whatWeNeed = new PickupObject[4];
			for (int point = 0; point < 4; point++)
			{
				if (GameManager.instance.rooms[room].placementPoints[point].currentObject != null)
				{
					whatWeHave[point] = GameManager.instance.rooms[room].placementPoints[point].currentObject.GetComponent< PickupObject>();
					//USING THIS MOMENT TO GET THE NAME.
					names[point] = string.Format("{0}", GameManager.instance.rooms[room].placementPoints[point].currentObject.name);
				}
				else
				{
					//NO OBJECT THERE. PUT IN A EMPTY NAME.
					names[point] = "";
				}
				if (GameManager.instance.rooms[room].placementPoints[point].targetObject != null)
				{
					whatWeNeed[point] = GameManager.instance.rooms[room].placementPoints[point].targetObject.GetComponent<PickupObject>();
				}
			}
			//Compare the two lists. For each one in the list that the other contains, that object is in the right room.
			for (int compare = 0; compare < 4; compare++)
			{
				if (whatWeHave[compare] != null && whatWeNeed[compare] != null)
				{
					if (whatWeHave[compare] == whatWeNeed[compare])
					{
						//We have a match. The object is in the right room, position, and style.
						//SKIP EVERYTHING ELSE AND MARK ALL THREE PARTS OF THIS BOOL RIGHT
						correctRooms[compare] = true;
						correctPos[compare] = true;
						correctStyle[compare] = true;
					}
					else
					{
						//SOMETHING DOES NOT MATCH. LETS FIGURE OUT WHAT BOOLS TO TURN ON OFF BY CHECKING THINGS IN ORDER.
						//START BY CHECKING IF TYPE OF THING THAT WE HAVE IS SUPPOSED TO BE IN THE ROOM
						foreach(PickupObject p in whatWeNeed)
						{
							if (whatWeHave[compare].type == p.type)
							{
								correctRooms[compare] = true;
								//What we have's type is supposed to be in the room somwhere.
							}
						}
						//NOW, IF SOMETHING'S TYPE WAS SUPPOSED TO BE IN THE ROOM, WE FIGURE OUT IF IT IS IN THE RIGHT PLACE
						if (correctRooms[compare] == true)
						{
							if(whatWeHave[compare].type == whatWeNeed[compare].type)
							{
								//In the right position.
								correctPos[compare] = true;
							}
						}
						//IF SOMETHING'S TYPE WAS SUPPOSED TO BE IN THE ROOM, WE FIGURE OUT IF IT HAS THE RIGHT STYLE
						if (correctRooms[compare] == true)
						{
							if (whatWeHave[compare].style == whatWeNeed[compare].style)
							{
								//In the right position.
								correctStyle[compare] = true;
							}
						}
					}
				}
			}
			//Send this to the right UI element.
			rooms[room].PopulateItemNames(names);
			rooms[room].PopulateRoomHearts(correctRooms);
			rooms[room].PopulatePositionHearts(correctPos);
			rooms[room].PopulateStyleHearts(correctStyle);
		}
		//Calculate hearts
		int heartsEarned = 0;
		foreach(OneRoomUI room in rooms)
		{
			heartsEarned += room.numHearts();
		}
		heartCounter.text = string.Format("{0}/48", heartsEarned);
		if(heartsEarned == 48)
		{
			endGame = true;
			victory = true;
			time = 0;
		}
		stagger = 0;
		open = true;
	}

	public void CloseOverlay()
	{
		stagger = 0;
		open = false;
	}


	public void EndGame()
	{

	}
}
