﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceSequence : MonoBehaviour {

	public BounceInOut[] bouncers;
	public float sequencing;
	float time  = 0;
	public bool startSequence = false;
	public bool goAway = false;


	// Update is called once per frame
	void Update() {
		if(goAway == true)
		{
			for (int i = 0; i < bouncers.Length; i++)
			{
				if (time > sequencing * i)
				{
					bouncers[i].BounceOut();
				}
			}
		}
		else if (startSequence == true)
		{
			time += Time.deltaTime;
			for (int i = 0; i < bouncers.Length; i++)
			{
				if (time > sequencing * i)
				{
					bouncers[i].BounceIn();
				}
			}
		}
	}
}
