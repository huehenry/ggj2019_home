﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceInOut : MonoBehaviour {

	public bool bounceIn;
	public float lerpTimer;

    public bool paper = false;
    private bool soundPlayed = true;

	// Update is called once per frame
	void Update () {
		if (bounceIn == true)
		{
            if (soundPlayed == false)
            {
                soundPlayed = true;
                if (paper == false)
                {
                    SFXManager._sfx.PlayEffect("UI_PopOut");
                }
                else
                {
                    SFXManager._sfx.PlayEffect("UIOpen");
                }
            }
            lerpTimer += Time.deltaTime * 1.5f;
			if (lerpTimer > 0.8f)
			{
				lerpTimer = 1;
            }
			if (lerpTimer <= 0.5f)
			{

				//Move backwards from current to 1.25
				this.transform.localScale = Vector3.Lerp(new Vector3(1.25f, 1.25f, 1.25f), this.transform.localScale, 1 - 2 * lerpTimer);
			}
			else
			{
 
                //Move from current to 1
                this.transform.localScale = Vector3.Lerp(this.transform.localScale, Vector3.one, 2 * (lerpTimer - 0.5f));
			}
		}
		else
		{
            if (soundPlayed == false)
            {
                if (soundPlayed == false)
                {
                    SFXManager._sfx.PlayEffect("UI_PopOut");
                    soundPlayed = true;
                }
                soundPlayed = true;
            }
            lerpTimer += Time.deltaTime * 3;
			if (lerpTimer > 0.8f)
			{
				lerpTimer = 1;
			}
			if (lerpTimer <= 0.5f)
			{
				//Move backwards from current to 1.25
				this.transform.localScale = Vector3.Lerp(new Vector3(1.25f, 1.25f, 1.25f), this.transform.localScale, 1 - 2 * lerpTimer);
			}
			else
			{
				//Move from current to 0
				this.transform.localScale = Vector3.Lerp(this.transform.localScale, Vector3.zero, 2 * (lerpTimer - 0.5f));
			}
		}
	}

	public void BounceIn()
	{
		if (bounceIn == false)
		{
            soundPlayed = false;
			bounceIn = true;
			lerpTimer = 0;
		}
	}

	public void BounceOut()
	{
		if (bounceIn == true)
		{
            soundPlayed = false;
            bounceIn = false;
			lerpTimer = 0;
		}
	}
}
