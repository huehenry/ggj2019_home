﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWiggle : MonoBehaviour {

	public float timeBetween = 0.2f;
	public float maxRot;
	public float current;

	public void Start()
	{
		current = Random.Range(0, timeBetween);
	}

	// Update is called once per frame
	void Update () {
		current += Time.deltaTime;
		if(current>=timeBetween)
		{
			this.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-maxRot, maxRot)));
			current = 0;
		}
	}
}
