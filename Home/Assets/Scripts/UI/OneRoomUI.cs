﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OneRoomUI : MonoBehaviour {

	public enum states
	{
		offScreen,
		updateText,
		updateRoomHearts,
		bringInPosStyleBases,
		updatePosAndStyleIndicators,
		onScreen
	}

	[Header("Basic Stuffs")]
	public states currentState;

	[Header("Object Names")]
	public Text[] objectNames;
	public BounceInOut[] objectNameBouncers;

	[Header("Room Indicators")]
	public BounceInOut[] roomHeartBouncers;

	[Header("Position and Style Indicators")]
	public BounceInOut[] posAndStyleBGBouncers;
	public BounceInOut[] posHeartBouncers;
	public BounceInOut[] styleHeartBouncers;

	private string[] currentObjectNames;
	private bool[] roomHeartsOn;
	private bool[] posHeartsOn;
	private bool[] styleHeartsOn;

	public bool onScreen;

	private void Awake()
	{
		currentObjectNames = new string[4];
		roomHeartsOn = new bool[4];
		posHeartsOn = new bool[4];
		styleHeartsOn = new bool[4];
	}

	// Update is called once per frame
	void Update() {

			switch (currentState)
			{
				case states.offScreen:
					//Waiting for something to change the states
					break;
				case states.updateText:
					bool allTextUpdated = true;
					for (int i = 0; i < currentObjectNames.Length; i++)
					{
						if (currentObjectNames[i] != objectNames[i].text)
						{
							//Shrink down
							objectNameBouncers[i].BounceOut();
							if (objectNameBouncers[i].lerpTimer>=1)
							{
								//Shrunk down fully. Change the name
								objectNames[i].text = currentObjectNames[i];
							}
							allTextUpdated = false;
						}
						else
						{
							//Bounce up
							objectNameBouncers[i].BounceIn();
						}
					}
					foreach(BounceInOut b in objectNameBouncers)
					{
						if(b.lerpTimer<1)
						{
							allTextUpdated = false;
						}
					}
					if(allTextUpdated == true)
					{
						currentState = states.updateRoomHearts;
					}
					break;
				case states.updateRoomHearts:
					//If the hearts are not on and should be, turn them on. If they are on and should be off, turn them off.
					bool roomHeartsUpdated = true;
					for (int i = 0; i < roomHeartsOn.Length; i++)
					{
						if (roomHeartsOn[i] == true)
						{
							if (roomHeartBouncers[i].bounceIn == false)
							{
								//This heart is on but showing as off. Bounce it in!
								roomHeartBouncers[i].BounceIn();
							}
						}
						else
						{
							if (roomHeartBouncers[i].bounceIn == true)
							{
								//This heart is off but it is shown as on. Get rid of it.
								roomHeartBouncers[i].BounceOut();
							}
						}
					}
					foreach (BounceInOut b in roomHeartBouncers)
					{
						if (b.lerpTimer < 1)
						{
							roomHeartsUpdated = false;
						}
					}
					if (roomHeartsUpdated == true)
					{
						currentState = states.bringInPosStyleBases;
					}
					break;
				case states.bringInPosStyleBases:
					//If the heart in this position is on, scale in the posStyleBase
					bool posStyleBaseUpdated = true;
					for (int i = 0; i < posAndStyleBGBouncers.Length; i++)
					{
						//We compare this to room hearts on.
						if (roomHeartsOn[i] == true)
						{
							//The room heart is on. If the check mark areas are showing off, turn them on.
							if (posAndStyleBGBouncers[i].bounceIn == false)
							{
								posAndStyleBGBouncers[i].BounceIn();
							}
						}
						else
						{
							if (posAndStyleBGBouncers[i].bounceIn == true)
							{
								//This heart is off so the bg needs to be off too.
								posAndStyleBGBouncers[i].BounceOut();
							}
						}
					}
					foreach (BounceInOut b in posAndStyleBGBouncers)
					{
						if (b.lerpTimer < 1)
						{
							posStyleBaseUpdated = false;
						}
					}
					if (posStyleBaseUpdated == true)
					{
						currentState = states.updatePosAndStyleIndicators;
					}
					break;
				case states.updatePosAndStyleIndicators:
					//Do these two together.
					bool posStyleHeartsUpdated = true;
					for (int i = 0; i < posAndStyleBGBouncers.Length; i++)
					{
						
						if (posHeartsOn[i] == true)
						{
							//The room heart is on. If the check mark areas are showing off, turn them on.
							if (posHeartBouncers[i].bounceIn == false)
							{
								posHeartBouncers[i].BounceIn();
							}
						}
						else
						{
							if (posHeartBouncers[i].bounceIn == true)
							{
								//This heart is off so the bg needs to be off too.
								posHeartBouncers[i].BounceOut();
							}
						}
						if (styleHeartsOn[i] == true)
						{
							//The room heart is on. If the check mark areas are showing off, turn them on.
							if (styleHeartBouncers[i].bounceIn == false)
							{
								styleHeartBouncers[i].BounceIn();
							}
						}
						else
						{
							if (styleHeartBouncers[i].bounceIn == true)
							{
								//This heart is off so the bg needs to be off too.
								styleHeartBouncers[i].BounceOut();
							}
						}
					}
					foreach (BounceInOut b in styleHeartBouncers)
					{
						if (b.lerpTimer < 1)
						{
							posStyleHeartsUpdated = false;
						}
					}
					foreach (BounceInOut b in posHeartBouncers)
					{
						if (b.lerpTimer < 1)
						{
							posStyleHeartsUpdated = false;
						}
					}
					if (posStyleHeartsUpdated == true)
					{
						currentState = states.onScreen;
					}
					break;
			}
	}

	public void PopulateItemNames(string[] itemsCurrentlyInRoom)
	{
		currentObjectNames = itemsCurrentlyInRoom;
	}

	public void PopulateRoomHearts(bool[] heartsOn)
	{
		roomHeartsOn = heartsOn;
	}

	public void PopulatePositionHearts(bool[] heartsOn)
	{
		posHeartsOn = heartsOn;
	}

	public void PopulateStyleHearts(bool[] heartsOn)
	{
		styleHeartsOn = heartsOn;
	}

	public int numHearts()
	{
		int returner = 0;
		for(int i = 0; i < 4; i++)
		{
			if(roomHeartsOn[i] == true)
			{
				returner += 1;
			}
			if (posHeartsOn[i] == true)
			{
				returner += 1;
			}
			if (styleHeartsOn[i] == true)
			{
				returner += 1;
			}
		}
		return returner;
	}
}
